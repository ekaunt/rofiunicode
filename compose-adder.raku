#!/usr/bin/env perl6
my %specials = 
		'Multi_key' => '⎄',
		'percent' => '%',
		'ampersand' => '&',
		'minus' => '-',
		'underscore' => '_',
		'greater' => '>',
		'less' => '<',
		'comma' => ',',
		'period' => '.',
		'dollar' => '$',
		'exclam' => '!',
		'question' => '?',
		'plus' => '+',
		'slash' => '/',
		'numbersign' => '#',
		'at' => '@',
		'bar' => '|',
		'grave' => '`',
		'asciitilde' => '~',
		'asciicircum' => '^',
		'parenleft' => '(',
		'parenright' => ')',
		'bracketleft' => '[',
		'bracketright' => ']',
		'braceleft' => '{',
		'braceright' => '}',
		'apostrophe' => "'",
		'quotedbl' => '"',
		'backslash' => "\\", 
		'colon' => ':',
		'semicolon' => ';',
		'equal' => '=',
		'space' => ' ',
		'asterisk' => '*',
		'Up' => '↑',
		'Down' => '↓',
		'Right' => '→',
		'Left' => '←',
		'space' => '␣',
		'★' => '★',
		;

unit sub MAIN ( 
	Str:D :l(:list($list))! where *.IO.f,  #= rofiunicode unicode character list file
); 


# find all the files that ~/.XCompose links to, and add them to a list of files that should be searched
my @files;
for grep /^include/, "$*HOME/.XCompose".IO.lines {
	my $xcompose = $_;
	$xcompose ~~ s/include //;
	$xcompose ~~ s|'%H'|$*HOME|;
	$xcompose ~~ s|'%L'|/usr/share/X11/locale/en_US.UTF-8/Compose|;
	$xcompose ~~ s:g/'"'//;
	@files.push: $xcompose.trim;
}

#| convert unicode code point to its character
sub cp2chr($uni) {
	S:i/U(<[A..F0..9]>**4..6)/{$0.Str.parse-base(16).chr}/ given $uni.Str
}

#| replace each symbol with its named counterpart, or its codepoint
sub replace($_) {
	state %cache; # caches the symbols so it doesnt need to recompute each character (30% faster)
	# remove all "<,>,␣" to only get the actual keys
	.split(/<[<>\ ]>/,:skip-empty).map({
		my ($key, $value) = $_, "";
		# return chached key if exists, else cache it then return 
		# (by replacing it with its symbol or the letter itself)
		%cache{$key} //= %specials{$key} // cp2chr($key) // $key
	}).join("");
}


my %compose;
@files.map({ for .IO.lines { unless /^\s*<[#]>/ or /^\s*$/ {
	my $split = .split(":")[1];
	my $uni = $split.match(/[<?after U><[A..F0..9]>**4..6]||[<?after '"'>.+?<?before '"'>]/) if $split;
	%compose{cp2chr($uni)}.push: replace(.split(":")[0].trim) if $uni;
}}
});

my %merged;
for $list.IO.lines -> $line {
	my ($char, $codepoint, $name, $rest) = $line.split: "\t", 4;
	if %compose{$char}:exists {
		%merged{$char} = ($codepoint,$name,%compose{$char}.join(",")).join("\t");
	} else {
		%merged{$char} = ($codepoint,$name).join("\t");
	}
	%merged{$char} ~= "\t$rest" if $rest;
}
say %merged.sort.join("\n");
