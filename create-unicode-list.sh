#!/bin/zsh
# try with bash, but i give no guarantees it'll work

usage(){
	echo -e << EOF
	USAGE: ucd.sh [-n|--nameslist NamesList.txt] [-u|--unihan ucd.unihan.flat.xml] [-i|--ids ids.txt] [-c|--compose]

	optional arguments are for custom files/script
	without specifying anything, it'll download the files from the internet

	-c|--compose 		Add all compose sequences to its character in unicode-list.txt
EOF
	exit 3
}

# handle input arguments
# ^: means it silences error on unsupported options (so i can deal with it myself)
# h: means it expects a parameter
# h:: means parameter is optional
while getopts ":n::u::i::ch" arg; do
	case "$arg" in
		n|--nameslist)
			nameslist="$OPTARG"
			echo nameslist specified at "$OPTARG"
			#shift # past argument
			#shift # past value
			;;
		#b|--blocks_add)
			#blocks_add="$OPTARG"
			#echo blocks_add specified at "$OPTARG"
			##shift # past argument
			##shift # past value
			#;;
		u|--unihan)
			unihan="$OPTARG"
			echo unihan specified at "$OPTARG"
			#shift # past argument
			#shift # past value
			;;
		i|--ids)
			ids="$OPTARG"
			echo ids specified at "$OPTARG"
			#shift # past argument
			#shift # past value
			;;
		c|--compose)
			# the + is to check if it exists
			compose="set"
			echo will add compose sequences to unicode-list-⎄.txt
			#shift # past argument
			#shift # past value
			;;
		h|--help)
			usage
			;;
		* )
			echo bad option
			#POSITIONAL+=("$1")
			#shift # past argument
			usage
			;;
	esac
done
#set -- "${POSITIONAL[@]}" # restore positional parameters


if [[ ! -v nameslist ]]; then
	echo "Downloading NamesList.txt"
	curl --output 'NamesList.txt' https://www.unicode.org/Public/UCD/latest/ucd/NamesList.txt
	nameslist='NamesList.txt'
fi
if [[ ! -v blocks_add ]]; then
	if [[ -f $(dirname $(which $0))/blocks_add.py ]]; then
		blocks_add=$(dirname $(which $0))/blocks_add.py
	else
		blocks_add='blocks_add.py'
	fi
fi
if [[ ! -v unihan ]]; then
	echo "Downloading ucd.unihan.flat.xml"
	curl https://www.unicode.org/Public/UCD/latest/ucdxml/ucd.unihan.flat.zip | jar x
	sed -i -e 's#<ucd .*#<ucd>#g' ucd.unihan.flat.xml
	unihan="ucd.unihan.flat.xml"
fi
if [[ ! -v ids ]]; then
	echo "Downloading ids.txt"
	curl --output 'ids.txt' https://raw.githubusercontent.com/LachlanAndrew/cjkvi-ids/master/ids.txt
	ids='ids.txt'
fi

# remove the first 29 lines, then all lines listing an empty block
sed -i -e '1,12d;/@@\t/d' "$nameslist"

echo 'adding chars to codepoints' # (can take 5m)'
# add in the chars next to the codepoints. takes ~3-5m!!
## pv makes a progress bar, cuz of the wait-time
## IFS= is so that it keeps the whitespace (i want this so i keep the tab
## in front of the lines that need to be joined with the one before it)
##pv "$nameslist" | while IFS= read -r line; do
	##if [[ $line =~ '^[0-9A-F]{4,6}' ]]; then
		### & replaces the entire match (not just match 1 or 2)
		##sed -e 's/^[0-9A-F]\{4,6\}/\\U&\t&/' <(echo "$line")
	##else
		###sed -e 's/ \([0-9A-F]\{4,6\}\)\>/ \\U\1 \1/g' <(echo "$line")
		##perl -pe 's#(?<!ISO|DIN|IEC) ([0-9A-F]{4,6})\b# \\U\1 \1#g;s/^\tx/\t→/' <(echo "$line")
	##fi
##done |

perl -i -X -CDS -MUnicode::Char -pe '
my $u = Unicode::Char->new();
if(/^[0-9A-F]{4,6}/ and not /^FFFF\b/){
	s{(^[A-F\d]{4,6})\b}{$c=$u->u($1);"$c\t$1"}e
}else{
	s{(?<!ISO|DIN|IEC) ([A-F\d]{4,6})\b}{$c=$u->u($1);" $c $1"}eg;
	s/^\tx/\t\N{U+2192}/
}' "$nameslist"

##to join all separated lines together
#vim "+g/^\t/normal! kJ" "+exit" "$nameslist"
## no idea why this works, but sed is faster and more portable than vim, so sed wins
sed -i ':a;N;/\n\t/s/\n\t/ /;ta;P;D' "$nameslist"

## ↓ to put the @ symbols onto every line (to add block info to every char)
echo "adding blocks & extras to lines"

python -c '
import re, sys

if sys.argv[1]:
	file = sys.argv[1]
else:
	print("please specify the path to NamesList.txt")

# # sed -i -e '"'"'1,29d'"'"' NamesList.txt
 # cmd = "sed -i -e '"'"'1,29d'"'"' "+file
 # subprocess.call(cmd.split())
 # cmd = '"'"'vim "+g/^[^0-9A-F@]/normal! kJ" "+exit"'"'"'+file
 # subprocess.call(cmd.split())
# # in vim: vim "+g/^[^0-9A-F@]/normal! kJ" "+exit" NamesList.txt #to join all separated lines together
# # ↓ to put the @ symbols onto every line (to add block info to every char)

with open(file) as f:
	content = f.readlines()

content = [x.strip() for x in content]
 
# match lines that dont start with a unicode codepoint
indeces_to_delete=[]
line=""
for i in range(len(content)):
	if re.match(r"^[@]", content[i]) and re.match(r"^[@]", content[i-1]):
		continue
	index = 0
	while re.match(r"^[@]", content[i+index]):
		if re.match(r"^[^@]", content[i-1]):
			line=""
		line += content[i+index][2:]
		indeces_to_delete.append(i+index)
		index+=1
		if len(content) < i+index:
			break
	content[i]+=line

for index in sorted(list(set(indeces_to_delete)), reverse=True):
	del content[index]

# [print(item) for item in content]
with open(file, "w") as f:
	for item in content:
		f.write("%s\n" % item)
' "$nameslist"


## add the char to all codepoints
#sed -e 's/\<\([0-9A-F]\{4,6\}\)\>/\\U\1\t\1/' < "$nameslist" | while read -r line;do echo -e "$line";done | sponge "$nameslist"
#sed -e 's/\<\([0-9A-F]\{4,6\}\)\>/\\U\1 \1/2g' < "$nameslist" | while read -r line;do echo -e "$line" || : ; done | sponge "$nameslist"
#iconv --to="UTF-8//IGNORE" "$nameslist" | sponge "$nameslist"

#remove first char from file (the NULL char U+0000) cuz it trips up programs like grep and diff (it 
#makes them think its a binary file and not a utf-8 file)
sed -i '1s/^.//' "$nameslist"

# ADD UNIHAN
# clean up the uni Han file
echo "adding unihan"
#only add the word "Definition" if the field exists
# TODO: ADD kStrange FOR UNICODE 14
xml select -t -m '/ucd/repertoire/char' -nl -v "concat(@cp,' ')" \
	--if "boolean(@kDefinition)" \
		-v "concat('Definition: ',@kDefinition,' | ')" -b \
	--if "boolean(@kTraditionalVariant)" \
		-v "concat('Traditional: ',@kTraditionalVariant,' | ')" -b \
	--if "boolean(@kSimplifiedVariant)" \
		-v "concat('Simplified: ',@kSimplifiedVariant,' | ')" -b \
	--if "boolean(@kSemanticVariant)" \
		-v "concat('Semantic: ',@kSemanticVariant,' | ')" -b \
	--if "boolean(@kSpecializedSemanticVariant)" \
		-v "concat('SemanticAlt:',@kSpecializedSemanticVariant,' | ')" -b \
	--if "boolean(@kCompatibilityVariant) and @kCompatibilityVariant != ''" \
		-v "concat('Compatibility: ',@kCompatibilityVariant,' | ')" -b \
	--if "boolean(@kZVariant)" \
		-v "concat('ZVariant: ',@kZVariant,' | ')" -b \
	--if "boolean(@kSpoofingVariant)" \
		-v "concat('Spoof: ',@kSpoofingVariant,' | ')" -b \
	--if "boolean(@kJapaneseKun)" \
		-v "concat('Kunyomi: ',@kJapaneseKun,' | ')" -b \
	--if "boolean(@kJapaneseOn)" \
		-v "concat('Onyomi: ',@kJapaneseOn,' | ')" -b \
	--if "boolean(@kKorean) or boolean(@kHangul)" \
		-v "concat('Korean: ',@kHangul,' ',@kKorean,' | ')" -b \
	--if "boolean(@kTotalStrokes)" \
		-v "concat('Strokes: ',@kTotalStrokes,' | ')" -b \
"$unihan" | sed 's# | $##' | sponge "$unihan"
#(`sponge` is cuz `> example.txt` starts writing to that file, before the command before `> example.txt` starts reading it. So as there was no output yet, example.txt is empty, the command reads an empty file. sponge soaks up all its input before opening the output file)

# delete all empty lines | add the codepoint to all codepoints | save&quit
# now doing it with perl
#vim '+g/^[0-9A-F]\{4,6}$\|^$/d' \
	#'+%s/\(\ \=U+\([0-9A-F]\{4,6}\).\{-}\(\ \|$\)\)/\ \\U\2 \2\3/g' \
	#'+%s/^\([0-9A-F]\{4,6}\)\ /\\U\1\t\1\t/' \
	#'+exit' "$unihan"
echo 'adding chars to unihan codepoints'
perl -CDS -ne '/^[A-F\d]{4,6}$|^$/ or print' "$unihan" |
perl -X -CDS -MUnicode::Char -pe '
	my $u = Unicode::Char->new();
	s#(\ ?U\+([A-F\d]{4,6}).*?(\ |$))#$c=$u->u($2);" $c $2$3"#ge;
	s#^([A-F\d]{4,6})\ #$c=$u->u($1);"$c\t$1\t"#e'|
sponge "$unihan"
#pv "$unihan" | perl -CDS -ne '/^[A-F\d]{4,6}$|^$/ or print' |
#perl -CDS -pe 's#(\ ?U\+([A-F\d]{4,6}).*?(\ |$))# \\U$2 $2$3#g;
		  #s#^([A-F\d]{4,6})\ #\\U$1\t$1\t#' |
## convert the \UXXXX[XX] into its chars
#while IFS= read -r line;do
	#echo "$line"
##(`sponge` is cuz `> example.txt` starts writing to that file, before the command before `> example.txt` starts reading it. So as there was no output yet, example.txt is empty, the command reads an empty file. sponge soaks up all its input before opening the output file)
#done | sponge "$unihan"

# remove extra info after hangul
perl -i -ple 's@([가-힣]):.*?\s|$@\1 @g;s@(^\ |\s$)@@' "$unihan"

#add IDS
echo "adding IDS to unihan"
raku -e '
sub MAIN ( :$ids! where *.IO.e, :$unihan! where *.IO.e ) {
	my %merged;

	for $unihan.IO.lines -> $line {
		my ($char, $codepoint, $v) = $line.split: "\t";
		%merged{$codepoint} = $char, $v;
	}
	for $ids.IO.lines -> $line {
		my ($codepoint, $char, $v) = $line.split: "\t", 3;
		# remove the "U+" from the codepoint
		$codepoint ~~ s/U\+(<[A..Fa..f0..9]>**4..6)/$0/;
		if %merged{$codepoint}:exists {
			%merged{$codepoint}[1] ~= " | " if %merged{$codepoint}[1].chars > 0;
			%merged{$codepoint}[1] ~= "IDS: $v";
		}
	}

	say %merged.map({
		(.value[0], .key, .value[1..*]).join("\t")
	}).join("\n");

	# adding hangul syllables
	for 0xAC00..0xD7A3 {
		say (.chr, .base(16), .uniname).join("\t")
	}
}
' --ids="$ids" --unihan="$unihan" | sponge "$unihan"

## EGYPTIAN HIEROGLYPHICS
echo "adding hieroglyphs"
curl -s https://en.wikipedia.org/wiki/List_of_Egyptian_hieroglyphs |
	sed ':a;N;$!ba;s#\n# #g;s#\s<\/td>#<\/td>#g' |
	perl -CDS -lpe 's#U\+([0-9A-F]{4,6})#\1#g' |
	# only necessary until Wikipedia fixes their website
	perl -CDS -lpe 's#(<input type="hidden".*?(?<!\/))>#$1/>#' |
	xml select -T -t -m '//*[@id="mw-content-text"]/div/table[2]/tbody/tr' -nl \
	-v "concat(td[1],'	', td[3],'	')" \
	--if "boolean(td[4][normalize-space()])" \
		-v "concat('Definition: ',td[4][normalize-space()],' | ')" -b \
	--if "boolean(td[5][normalize-space()])" \
		-v "concat('Transliteration: ',td[5][normalize-space()],' | ')" -b \
	--if "boolean(td[6][normalize-space()])" \
		-v "concat('Phonetic: ',td[6][normalize-space()],' | ')" -b \
	--if "boolean(td[7][normalize-space()])" \
		-v "concat('Notes: ',td[7][normalize-space()],' | ')" -b |
	sed 's# | $##' |
	perl -CDS -n -e 'print unless /(.\s[0-9A-F]{4,6}\s$)|(^\s*$)/' > hieroglyphs



## Finish up

unicode_list="unicode-list.txt"
[[ -n $compose ]] && unicode_list="unicode-list-⎄.txt"

LC_ALL=C sort "$nameslist" "$unihan" hieroglyphs emojis.txt > "$unicode_list"

# if $compose is set (if its not empty) then add the compose sequences
if [[ -n $compose ]]; then
	echo "adding compose sequences"
	raku "$(dirname $(which $0))/compose-adder.raku" --list="$unicode_list" | sponge "$unicode_list"
fi

rm "$nameslist" "$unihan" "$ids" hieroglyphs 
mv "$unicode_list" "$(dirname $(which $0))/$unicode_list"
echo "DONE!"
