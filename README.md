# rofiunicode — a unicode picker for rofi

This is a fork of [rofiunicode by matf](https://git.teknik.io/matf/rofimoji) with the addition of over 130,000 unicode characters. These additions include all of unihan with tons of information like the definitions of characters, simplified/tradition variants, Japanese/Korean readings and even the IDS, and all the Egyptian hieroglyphics with tons of information on them from [Gardiner's sign list](https://en.wikipedia.org/wiki/List_of_Egyptian_hieroglyphs).

Recommended command line to run rofiunicode from a keybinding:
```bash
python3 /path/to/rofiunicode/rofiunicode.py --use-clipboard -s ask --rofi-args="-columns 1; -width 50%; -no-click-to-exit; -theme sidebar"
```

A dmenu picker has also been provided

```bash
./dmenu-unicode
```


### How does it look?
![Screenshot of rofiunicode](screenshot1.png)

![Screenshot of rofiunicode](screenshot2.png)

The theme used in the above screenshot is *solarized-light* (included with rofiunicode) and *sidebar by qball*, which can be selected as default using `rofi-theme-selector` in terminal.

---

How often did you want to insert one of those Unicode emoji only to learn that there is no nice picker for Linux?
Fear no more, this script uses the power of [rofi](https://github.com/DaveDavenport/rofi/) to present exactly the picker you always wanted.
Inserts the selected emoji directly, or copies it to the clipboard.

## Usage
1. Run `rofiunicode.py`
2. Search for the character or emoji you want
3. (optional) Select multiple characters with `shift+enter`
4. Hit `enter` to copy the character \
   `alt+t` or `alt+p` can be used to select a specific input method
5. Hit `alt+enter` to copy the character and the entire description
6. Maybe select a skin color
7. 🎠

By default, `rofiunicode` uses `xdotool type`. To choose to spam your clipboards, you can either use the keybinding `alt+p` or start it as `rofiunicode --use-clipboard` (`-c`). If you want to use typing, you can hit `alt+t`, even though it was started with `--use-clipboard`.

## Configuration
You can choose a skin tone with the `--skin-tone` (or `-s`) parameter. The available values are `light`, `medium-light`, `moderate`, `dark brown`, `black`, as well as `neutral` and `ask` to be shown the prompt (this is also the default).

If you have any arguments for rofi, you can make `rofiunicode` pass them through like this: `rofiunicode --rofi-args="-columns 3"`.

You can also define your own set of emojis (or whatever) and use `rofiunicode` to pick them by providing the `--emoji-file` (`-f`) parameter. This could be helpful if you want them ordered in some way, only use a subset or if you want non-English descriptions.

## Installation

### From source

```bash
git clone https://gitlab.com/ekaunt/rofiunicode.git
mv rofiunicode ~/.local/share
```
then you can run it by doing

```bash
~/.local/share/rofiunicode/rofiunicode.py
```

What else do you need:
- Python 3
- A font that can display emoji, for example [EmojiOne](https://github.com/emojione/emojione) or [Noto Emoji](https://www.google.com/get/noto/)
- xdotool for typing the emoji
- xsel to copy the emoji to the clipboard

For Ubuntu zesty: `sudo aptitude install fonts-emojione python3 rofi xdotool xsel` \
For Arch: `sudo pacman -Syu emoji-font python rofi xdotool xsel`

## Updating the emojis
This is only needed if a new Unicode version came out and you can't wait for the official update!

Just run this ↓ and you'll be all set.

```bash
cd ~/.local/share/rofiunicode
./create-unicode-list.sh
```

Or you can specify `-c` to generate a `unicode-list-⎄.txt` file which will show the [compose sequences](https://en.wikipedia.org/wiki/Compose_key) your computer uses ([like this one](https://github.com/grenzionky/xcompose)) by each character that has one.

```bash
cd ~/.local/share/rofiunicode
./create-unicode-list.sh -c
```

you also need to enable to new `unicode-list-⎄.txt` file by adding `--compose` to the argument list

```bash
	~/.local/share/rofiunicode/rofiunicode.py --use-clipboard --skin-tone="ask" --compose --rofi-args="-columns 1; -width 50%; -no-click-to-exit; -theme sidebar"
```

