
my %e;

for "emojis.txt".IO.lines { 
	my ($char, $desc) = $_.split: "\t", 2; 
	my $cp = $char.ords».base(16);
	%e{$char} = "$cp\t$desc";
}

 for "emojis.tg".IO.lines { 
	my ($char, $desc) = $_.split: "\t", 2; 
	# remove the emoji variation selector if its there
	if $char.ords.contains(65039) {
		$char = $char.ords[0...*-1].chrs;
	}
	if %e{$char}:exists {
		 %e{$char} ~= " " ~ $desc;
	} else { 
		my $cp = $char.ords».base(16);
		%e{$char} = "$cp\t$desc";
	} 
}

#put %e;
"combined-emojis".IO.spurt: %e;
